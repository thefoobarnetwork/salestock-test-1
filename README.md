# Sale Stock Indonesia

The project is submitted as part of Sale Stock engineering team test. It recreates the user experience for Sale Stock's web application.

## Running the app

To run the app locally: just clone it, install the packages and then you are good to go. Alternatively, you can visit a demo of the app on this [Firebase hosting](https://countr-app-p90d.firebaseapp.com/category/dress).

```
$ git clone git@bitbucket.org:thefoobarnetwork/salestock-test-1.git
$ cd salestock-test-1/
$ npm install
$ npm run start
```

## Technical Stack

> This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

The app is built on top of the following stack:

* ReactJS
* Redux
* React Router
* Firebase
* Semantic UI

# The submitted work

The following are the requirements given:

* User can browse products in infinite list
* User can view detailed information of each product
* User can view the image of each product
* A big plus if you can add CMS for product catalog

And the submitted work has to follow the following rules:

* You must use JavaScript and React as view engine, additional library and framework.
* Please use first commit to initialize framework/libraries only, and the subsequent commits with proper commit logs.
* You must submit your solution in public git repository.
* Your application must be able to run locally. Please have reasonable step (no more than 5 steps) and provide necessary readme to run your app.

Finally, the assessment is done based on these:

* Code cleanliness
* Application and data model design and abstraction layer
* Runnable locally in other local development machine
* Optional: Quality assessment with unit test and or functional API test
* Optional: Huge plus for adding “CMS for product catalog”

## Summary of the work

Given the requirements, I am able to fulfill all but the last one which is the CMS for product catalog. However, I should also inform that the completion of the requirements varies. I.e. it might not be as good as what is expected by the reviewer.

The work is all done with React and JavaScript. It connects to a Firebase backend. There are no tests in place. I am still new to writing tests for development.

I believe my code is quite clean, in the sense that it should be relatively easy for other people to read and understand. I also put a lot of in-line comments on my code, which also act as a self-document for the code base.

## Review of the requirements

In this section will try to sum up the requirements that I tried to achieve.

### User can browse products in infinite list

This should work as expected. It should show an empty state when there are no products returned from backend, as well as when it hits the end of the possible payload.

Playing this with Firebase is not exactly a straightforward process. I put comments regarding this matter within the code. Nonetheless, I abstracted the API call from within the component by making an interface for them to talk to each other.

### User can view detailed information of each product

I made some adjustment here. I don't really like scrolling vertically when viewing a detail of a product. This is because I might want to scroll back to the top at any given point.

A quick fix to the said problem might be putting a "back-to-top" button. But I don't think that is the solution.

So I decided to change the scrolls.

I use tabs to group informations accordingly. The use of tabs is intended to reduce the vertical scroll of the page. There are three tabs here: "Deskripsi", "Ukuran", and "Review".

> I know that at times tabs is being frowned upon by UI/X guys. However, I think this is one of the few cases where tabs actually is a good fit. It is quicker to find information by hitting the right tab instead of reading long, jumbled info paragraphs.

The "Deskripsi" tab contains a summary about the product. The "Ukuran" tab contains the size chart of the product. While the "Review" tab is actually just the comments. I think the comments are actually reviews, so I chose to rename the tab.

I also served the size chart in table. This helps to give a much clearer visibility of the sizes. People can just see what size they need and obtain the measurements from the table.

### User can view the image of each product

This is something that I believe would be highly debatable. I believe that the user should have access to all of the product's images from the get go. I know I would love to have that. I don't really like having to dive into a product's detail page just to see more images of the item. It's just one step too many.

So I decided to put all of the available images on the home page as well. The user can scroll horizontally to see more images of the product. I was going for Instagram-like experience, here. Unfortunately, I wasn't able to implement pinch-to-zoom for the image gallery.

The pinch-to-zoom idea was intended to replace the click-and-tap behavior we often see on image gallery libraries. I like its interaction, and the real-life like feeling it brings. As if you were interacting with this holographic interface, and you could pinch on the interface to zoom in and out. Pretty cool, eh?

### Other UX-related decisions

I decided to scrap more text/button CTAs from the UI and replace them with icon button CTAs. One thing that I learned from my designer friend is that icon represents words. Note that this could be a bad thing if the icon design is ambiguous.

Nonetheless, I believe things like "like", "share", "buy", and "more info"; those are universally recognizable by now. Everyone should be able to understand that, for example, a heart icon means "like". Or a shopping bag means "buy". So, icons for the win!

### The rough stuffs

I am not going to butter it up, this submitted work definitely looks like a bare proof of concept. It's far from complete. The navigation is jerky, the Redux state might cough up, the code is not optimized, etc. 

But I think it should be able to represent few things. My idea about good user interface and experience, and; my trains of thought when crafting the user interface and experience through coding.

Cheers!
