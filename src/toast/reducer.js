import types from './action'

const initialState = {
  show: false,
  type: null,
  header: null,
  content: '',
}

/**
 * Reducer for toast message.
 * @param {object} state - Store state
 * @param {object} action - Action dispatched
 */
export default function toast(
  state = {...initialState},
  action = { type: 'FOO', payload: {} }
) {
  const { type, payload } = action

  switch (type) {
    case types.DISMISS: return {...initialState}
    case types.ERROR_MESSAGE:
    case types.SIMPLE_MESSAGE:
    case types.SUCCESS_MESSAGE: return setToastMessage(payload)
    default: return state
  }
}

/**
 * Set toast message content.
 * @param {object} payload - The message payload
 */
function setToastMessage(payload) {
  const { type, content, header } = payload
  const state = {
    show: true,
    type,
    header: typeof header === 'string' ? header : null,
    content,
  }

  return state
}
