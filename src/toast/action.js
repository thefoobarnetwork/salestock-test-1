/** List of action names */
const types = {
  DISMISS: 'TOAST_DISMISS',
  ERROR_MESSAGE: 'TOAST_ERROR_MESSAGE',
  SIMPLE_MESSAGE: 'TOAST_SIMPLE_MESSAGE',
  SUCCESS_MESSAGE: 'TOAST_SUCCESS_MESSAGE',
}

/**
 * Dismiss toast message.
 */
const toastDismiss = () => ({ type: types.DISMISS })

/**
 * Show error toast.
 * @param {string} content - Error message
 * @param {string} code - Error code
 * @param {string} header - Optional header
 */
const toastErrorMessage = (content = '', code, header) => {
  console.log('ERROR CODE', code)
  console.log('ERROR MESSAGE', content)

  return {
    type: types.ERROR_MESSAGE,
    payload: { type: 'error', content, header }
  }
}

/**
 * Show simple toast message.
 * @param {string} content - Message
 * @param {string} header - Optional header
 */
const toastSimpleMessage = (content = '', header) => ({
  type: types.SIMPLE_MESSAGE,
  payload: { type: 'info', content, header }
})

/**
 * Show success toast.
 * @param {string} content - Success message
 * @param {string} header - Optional header
 */
const toastSuccessMessage = (content = '', header) => ({
  type: types.SUCCESS_MESSAGE,
  payload: { type: 'success', content, header }
})

/** List of action creators */
const actions = {
  toastDismiss,
  toastErrorMessage,
  toastSimpleMessage,
  toastSuccessMessage,
}

/** List of callable dispatchers */
const dispatchers = {
  /**
   * Reset toast on dismissed.
   */
  onDismiss: () => toastDismiss(),
}

export default types
export { actions, dispatchers }
