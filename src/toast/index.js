import React from 'react'
import { connect } from 'react-redux'

import ToastMessage from './ui'
import { dispatchers as mapDispatch } from './action'

/**
 * Map state to component's props.
 * @param {object} state - App's current state
 */
const mapState = state => ({ ...state.toast })

/**
 * Wrapper to mount the toast message correctly.
 * @param {boolean} show - Toggle the toast message
 */
const ToastWrapper = ({ show, ...props}) => (
  ! show ? null : <ToastMessage {...props} />
)

const Toast = connect(mapState, mapDispatch)(ToastWrapper)

export default Toast
