import React, { Component } from 'react'
import { Message } from 'semantic-ui-react'
import { string } from 'prop-types'
import './index.css'

class ToastMessage extends Component {
  static propTypes = {
    /** Message content for toast */
    content: string.isRequired,

    /** Optional header for toast */
    header: string,

    /** Toast type: error, success, none */
    type: string,
  }

  timer = null

  /**
   * Countdown to dismiss toast when mounted.
   */
  componentDidMount() {
    this.timer = setTimeout(() => {
      this.clearTimer()
      this.props.onDismiss()
    }, 5e3)
  }

  /**
   * Clear countdown when component is unmounted.
   */
  componentWillUnmount() {
    if (this.timer !== null) this.clearTimer()
  }

  /**
   * Reset the countdown.
   */
  clearTimer() {
    clearTimeout(this.timer)
    this.timer = null
  }

  /**
   * Render the component.
   */
  render() {
    const { header } = this.props
    let props = {
      content: this.props.content,
      // [this.props.type]: true,
      // size: 'large',
      color: 'black',
      onDismiss: this.props.onDismiss,
    }

    if (header) props.header = header

    return (
      <div className="ssk toast">
        <Message {...props} style={{ paddingRight: '32px' }} />
      </div>
    )
  }
}

export default ToastMessage
