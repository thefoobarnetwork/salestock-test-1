import { actions as toastActions } from '../toast/action'
import { DressAPI } from '../api'

/** List of action names */
const types = {
  IS_FETCHED: 'PRODUCT_IS_FETCHED',
  IS_FETCHING: 'PRODUCT_IS_FETCHING',
  NOT_FOUND: 'PRODUCT_NOT_FOUND',
  RESET_STATE: 'PRODUCT_RESET_STATE',
  SET_DATA: 'PRODUCT_SET_DATA',
  TOGGLE_LIKE: 'PRODUCT_TOGGLE_LIKE',
  TOGGLE_RELATED_LIKE: 'PRODUCT_TOGGLE_RELATED_LIKE',
}

/**
 * Flag product fetched.
 */
const productIsFetched = () => ({ type: types.IS_FETCHED })

/**
 * Flag product is fetching.
 */
const productIsFetching = () => ({ type: types.IS_FETCHING })

/**
 * Flag unindentified product ID.
 */
const productNotFound = () => ({ type: types.NOT_FOUND })

/**
 * Reset the app's state.
 */
const productResetState = () => ({ type: types.RESET_STATE })

/**
 * Set the data fetched from API to store.
 * @param {object} item - Product item
 * @param {object} related - Related products
 */
const productSetData = (item = {}, related = {}) => ({
  type: types.SET_DATA,
  payload: { item, related }
})

/**
 * Toggle product like.
 */
const productToggleLike = () => ({ type: types.TOGGLE_LIKE })

/**
 * Toggle like on related product.
 * @param {string} key - Reference key
 */
const productToggleRelatedLike = key => ({
  type: types.TOGGLE_RELATED_LIKE,
  payload: { key }
})

/** List of action creators */
const actions = {
  //
}

/** List of callable dispatchers */
const dispatchers = {
  /**
   * Clear the app's state.
   */
  clearState: () => productResetState(),

  /**
   * Fetch product data.
   * @param {string} productId - Firebase ID
   * @param {function} dispatch - Dispatch action
   */
  fetchProduct: productId => dispatch => {
    let item = {}
    let related = {}

    const fetchRelated = tagId => DressAPI.fetchRelatedProduct(tagId).then(
      /**
       * Assign related products.
       * @param {object} snapshot - Firebase snapshot
       */
      snapshot => {
        related = snapshot.val() || {}

        dispatch(productSetData(item, related))
        dispatch(productIsFetched())
      }
    ).catch(
      /**
       * Use toast to notify about the error.
       * @param {string} code - Error code
       * @param {string} message - Error message
       */
      ({ code, message}) => {
        dispatch(productIsFetched())
        dispatch(toastActions.toastErrorMessage(message, code))
      }
    )

    dispatch(productIsFetching())

    DressAPI.fetchProduct(productId).then(
      /**
       * Assign item and check for tags.
       * @param {object} snapshot - Firebase snapshot
       */
      snapshot => {
        item = snapshot.val() || {}

        if ( ! item.tags) {
          dispatch(productNotFound())
        } else {
          item.key = productId
          fetchRelated(item.tags)
        }
      }
    ).catch(
      /**
       * Use toast to notify about the error.
       * @param {string} code - Error code
       * @param {string} message - Error message
       */
      ({ code, message}) => {
        dispatch(productIsFetched())
        dispatch(toastActions.toastErrorMessage(message, code))
      }
    )
  },

  /**
   * Toggle the like state of the product.
   */
  toggleLike: () => productToggleLike(),

  /**
   * Toggle the like state of related product.
   * @param {string} key - Reference key
   */
  toggleRelatedLike: key => productToggleRelatedLike(key),
}

export default types
export { actions, dispatchers }
