import types from './action'

const initialState = {
  /** Flag that the product is being fetched */
  isFetching: true,

  /** Product item data */
  item: {},

  /** Flag that the product does not exist */
  notFound: false,

  /** Related product list */
  related: {},
}

/**
 * Reducer for product page.
 * @param {object} state - Store state
 * @param {object} action - Action dispatched
 */
function product(
  state = initialState,
  action = { type: 'FOO', payload: {} }
) {
  const { type, payload } = action

  switch (type) {
    case types.IS_FETCHED: return { ...state, isFetching: false }
    case types.IS_FETCHING: return { ...state, isFetching: true }
    case types.NOT_FOUND: return { ...state, isFetching: false, notFound: true }
    case types.RESET_STATE: return resetState()
    case types.SET_DATA:
      return { ...state, item: payload.item, related: payload.related }
    case types.TOGGLE_LIKE: return toggleLike(state)
    case types.TOGGLE_RELATED_LIKE: return toggleRelatedLike(state, payload)
    default: return state
  }
}

/**
 * Reset app's state.
 */
function resetState() {
  return {
    isFetching: true,
    item: {},
    notFound: false,
    related: {},
  }
}

/**
 * Togle like state.
 */
function toggleLike(state) {
  let item = {...state.item}

  item.liked = ! item.liked
  return { ...state, item }
}

/**
 * Toggle the like on related product.
 * @param {object} state - App state
 * @param {object} payload - Action payload
 */
function toggleRelatedLike(state, payload) {
  const { key } = payload
  let related = {...state.related}

  related[key].liked = ! related[key].liked
  return { ...state, related }
}

export default product
