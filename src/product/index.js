import ProductUI from './ui'
import { connect } from 'react-redux'
import { dispatchers } from './action'

const mapState = (state, ownProps) => ({
  ...state.product,
  ...ownProps,
})

export default connect(mapState, dispatchers)(ProductUI)
