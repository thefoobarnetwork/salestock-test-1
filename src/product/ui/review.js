import React from 'react'
import { object } from 'prop-types'

/**
 * Display user review.
 * @param {object} comments - List of comments
 */
const UserReviews = ({ comments }) => (
  <div className="ssk product-ui comments">{

    Object.keys(comments).map(key => (
      <div key={key} className="ssk product-ui user-comment">
        <div className="user-image" />
        <p className="user-name">John Smith</p>
        <p className="user-text">{comments[key].comment}</p>
      </div>
    ))

  }</div>
)

UserReviews.propTypes = {
  /** List of comments */
  comments: object,
}

export default UserReviews
