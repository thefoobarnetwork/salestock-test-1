import React from 'react'
import { object } from 'prop-types'
import { reOrderSizeMap } from '../../lib'

/**
 * Summary of the product.
 * @param {object} item - Product item
 */
const ProductDesc = ({ item }) => {
  const availability = reOrderSizeMap(item.size)
  const availKeys = Object.keys(availability)

  return (
    <div className="ssk product-ui desc">
      <ul>
        <li>
          Ukuran tersedia: <strong>{
            availKeys.map((key, i) => {
              let text = availability[key].alias

              if (i < availKeys.length - 1) text += ', '
              return text
            })
          }</strong>
        </li>
        <li>
          <strong>{item.try_first ? 'Bisa ' : 'Tidak bisa '}</strong>
          dicoba dulu
        </li>
        <li>Bahan: <strong>{item.description.fabric}</strong></li>
        <li>Warna: <strong>{item.description.color}</strong></li>
        <li>{item.description.additional}</li>
        <li>Model menggunakan ukuran {item.description.model_size}</li>
      </ul>
    </div>
  )
}

ProductDesc.propTypes = {
  /** Product item */
  item: object,
}

export default ProductDesc
