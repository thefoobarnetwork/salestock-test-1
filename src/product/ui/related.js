import React, { Component } from 'react'
import Flickity from 'flickity'
import { Header } from 'semantic-ui-react'
import { ProductCard } from '../../component'
import { func, object } from 'prop-types'

/**
 * Header for related section.
 */
const RelatedHeader = () => (
  <div className="pui-meta">
    <Header as={'h3'} content={'Lihat ini juga, Sis!'} />
    <p>
      Produk-produk ini sejenis dengan yang sedang Sis lihat,
      mana tahu minat?
    </p>
  </div>
)

/**
 * Carousel for related product.
 * @param {object} related - Product data
 * @param {function} toggleLike - Toggle like
 */
const RelatedCarousel = ({ related, toggleLike }) => (
  <div id="rwp-flk" className="related-wrapper carousel">
    {Object.keys(related).map(key => {
      let item = related[key]
      item.key = key

      return (
        <div key={key} className="rwp carousel-cell">

          <ProductCard
            item={item}
            productId={key}
            toggleLike={() => toggleLike(key)}
            noScroll={true}
          />

        </div>
      )
    })}
  </div>
)

/**
 * Show related products of the product.
 * @param {object} related - Related products
 * @param {function} toggleLike - Toggle like
 */
class RelatedProducts extends Component {
  static propTypes = {
    /** Related object data */
    related: object,

    /** Toggle like for related products */
    toggleLike: func,
  }

  /**
   * Initialize Flickity on related product carousel.
   */
  componentDidMount() {
    new Flickity(`#rwp-flk`, {
      adaptiveHeight: true,
      pageDots: true,
    })
  }

  /**
   * Render the component.
   */
  render() {
    return (
      <div className="ssk product-ui related">
        <RelatedHeader />
        <RelatedCarousel {...this.props} />
      </div>
    )
  }
}

export default RelatedProducts
