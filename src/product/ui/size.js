import React from 'react'
import {
  Table,
  TableBody,
  TableCell,
  TableHeader,
  TableHeaderCell,
  TableRow,
} from 'semantic-ui-react'
import { object } from 'prop-types'
import { reOrderSizeMap } from '../../lib'

/**
 * Head row for the table.
 * @param {array} keys - Size keys
 * @param {object} sizes - Size maps
 */
const ListHead = ({ keys, sizes }) => (
  <TableRow>
    <TableHeaderCell>&nbsp;</TableHeaderCell>

    {keys.map(key => (
      <TableHeaderCell key={key} className="ssk pvh">{
        sizes[key].alias
      }</TableHeaderCell>
    ))}
  </TableRow>
)

/**
 * Data row for the table.
 * @param {array} keys - Size keys
 * @param {object} sizes - Size maps
 * @param {string} metric - Size metric
 */
const ListData = ({ keys, sizes, metric }) => (
  <TableRow>
    <TableCell>{sizes.small[metric].alias}</TableCell>

    {keys.map(key => (
      <TableCell className="ssk pvx" key={key}>{
        sizes[key][metric].measurement
      }</TableCell>
    ))}
  </TableRow>
)

/**
 * Display the product size chart.
 * @param {object} sizes - Item size
 */
const ProductSize = ({ sizes }) => {
  const orderedSizes = reOrderSizeMap(sizes, true)
  // the metrics has to match the data of the product itself
  const metrics = Object.keys(orderedSizes.small).filter(item =>
    item !== 'alias' &&
    item !== 'available' &&
    item !== 'stock'
  )
  const sizeKeys = Object.keys(orderedSizes)

  return (
    <Table celled unstackable>
      <TableHeader>
        <ListHead keys={sizeKeys} sizes={orderedSizes} />
      </TableHeader>

      <TableBody>{
        metrics.map((metric, i) => (
          <ListData
            key={i}
            keys={sizeKeys}
            sizes={orderedSizes}
            metric={metric}
          />
        ))
      }</TableBody>
    </Table>
  )
}

ProductSize.propTypes = {
  /** Item size */
  sizes: object,
}

export default ProductSize
