import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { Tab, TabPane } from 'semantic-ui-react'

import ProductDesc from './desc'
import ProductSize from './size'
import RelatedProducts from './related'
import UserReviews from './review'
import { ProductCard } from '../../component'
import './index.css'

class ProductUI extends Component {
  /**
   * Fetch product on load.
   */
  componentDidMount() {
    const { productId } = this.props.match.params
    this.props.fetchProduct(productId)
  }

  /**
   * Clear state on removing component.
   */
  componentWillUnmount() {
    this.props.clearState()
  }

  /**
   * Render component UI.
   */
  render() {
    const { isFetching, notFound, item, related } = this.props
    const tabs = [{
      menuItem: 'Deskripsi',
      render: () => (
        <TabPane>
          <ProductDesc item={item} />
        </TabPane>
      ),
    }, {
      menuItem: 'Ukuran',
      render: () => (
        <TabPane>
          <p>Semua ukuran dalam <strong>sentimeter (cm)</strong></p>
          <ProductSize sizes={item.size} />
        </TabPane>
      ),
    }, {
      menuItem: 'Review',
      render: () => (
        <TabPane>
          <UserReviews comments={item.comments} />
        </TabPane>
      )
    }]

    return (
      isFetching ? null :

      notFound ? <Redirect to={'/throw-away-route'} /> :

      <div className="ssk product-ui wrapper">
        <ProductCard
          item={item}
          productId={item.key}
          toggleLike={this.props.toggleLike}
          noLink={true}
        />

        <div className="ssk product-ui info">
          <Tab panes={tabs} />
        </div>

        <RelatedProducts
          related={related}
          toggleLike={this.props.toggleRelatedLike}
        />
      </div>
    )
  }
}

export default ProductUI
