/**
 * Format the value to match currency format.
 * @param {string} value - Value string
 */
export function formatCurrency(value) {
  // split by the floating point
  // parse to string by adding '' to the value
  let float = ('' + value).split('.')
  // split value digits
  let _num = float[0].split('')
  // prepare for stringing
  let num = ''
  let nums = []
  // let finalNumber = ''

  // loop from the last digit, go in reverse
  for (let i = _num.length - 1; i >= 0; i--) {
    num = _num[i] + num

    // push to the `nums` array if there are already 3 items, OR
    // if the loop has reached the first digit
    if (num.length === 3 || i === 0) {
      nums.unshift(num)
      num = ''
    }
  }

  // RETURN NUMBER WITHOUT FLOATING POINT FOR NOW
  // UNCOMMENT THE LINES BELOW AND ITS CORRESPONDING VARIABLES UP TOP
  // IF YOU WANT TO RETURN FLOATING POINT
  // ------------------------------------
  // // string back using `,` to annotate the thousands etc
  // finalNumber += nums.join('.')
  // // put back the floating point, if available
  // finalNumber += typeof float[1] !== 'undefined' ? `,${float[1]}` : ''

  return nums.join('.')
}

/**
 * Capitalize words of a text.
 * @param {string} text - Text
 * @param {string} delimiter - Delimiter type
 */
export function capitalizeWords(text, delimiter = ' ') {
  return text.split(delimiter)
    .map(word =>
      word.charAt(0).toUpperCase() + word.slice(1, word.length)
    )
    .join(' ')
}

/**
 * Re-order the map size to be ordered by the actual sizes.
 * @param {object} itemSizes - Item sizes
 * @param {boolean} skipStock - Skip stock check
 */
export function reOrderSizeMap(itemSizes, skipStock = false) {
  /**
   * FYI
   * ---
   *
   * Every JSON payload in a request would have its keys sorted
   * alphabetically. This cause problem with the sizes. This is because
   * the size names are not alphabetically ordered. If we just straightforward
   * copy the JSON payload to the component's `sizes` state, we will get
   * an incorrect order of the sizes.
   *
   * Due to this problem, we choose to use a pre-defined `sizes` object with
   * correctly ordered sizes. We will then subtract (i.e. delete) the sizes
   * that are not available in stock.
   */
  let sizes = {
    extra_small: {},
    small: {},
    medium: {},
    large: {},
    extra_large: {},
    double_extra_large: {},
    triple_extra_large: {},
  }

  Object.keys(itemSizes).forEach(key => {
    const size = itemSizes[key]

    if ( ! skipStock && ! size.available) {
      // skip item if it's not available, and delete size key
      delete sizes[key]
      return
    }

    // otherwise push it to list of available sizes
    sizes[key] = size
  })

  /**
   * FYI
   * ---
   *
   * Still, the approach above causes another problem. The payload does not
   * always have all the keys defined in `sizes` variable. This is because
   * the payload is supposed to mimic the sizes in which the item is produced.
   *
   * This problem result in few empty objects still existing in the `sizes`.
   * Hence we have to run a final pruning to get the clean, ordered sizes
   * we need.
   */
  Object.keys(sizes).forEach(key => {
    // an object is empty if it does not have the payload of a `size`
    if (typeof sizes[key].alias === 'undefined') {
      // if so, remove the key
      delete sizes[key]
    }
  })

  return sizes
}
