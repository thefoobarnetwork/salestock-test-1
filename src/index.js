import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'

import App from './app'
import registerServiceWorker from './register-service-worker'
import store  from './store'

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
registerServiceWorker()

/**
 * TODO:
 *
 * 4. Go back to fix sorting and filtering.
 * 5. Fix redux store refreshing.
 * 6. Refactor code.
 */
