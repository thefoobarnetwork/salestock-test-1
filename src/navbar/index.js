import React from 'react'
import { Link } from 'react-router-dom'
import { Icon, Menu, MenuItem } from 'semantic-ui-react'

import { HOME_PATH } from '../config'
import './index.css'

/** Placeholder menu items */
const menuItems = [
  { name: 'heart' },
  { name: 'shopping bag' },
  { name: 'ellipsis horizontal' },
]

/**
 * Navigation bar component.
 */
const Navbar = () => (
  <div className="ssk app-menu wrapper">
    <div className="ssk app-menu inner">
      <Menu fluid widths={4}>
        <MenuItem as={Link} to={HOME_PATH}>
          <Icon name={'home'} size={'large'} />
        </MenuItem>

        {menuItems.map((item, i) => (
          <MenuItem key={i}>
            <Icon name={item.name} size={'large'} />
          </MenuItem>
        ))}
      </Menu>
    </div>
  </div>
)

export default Navbar
