import thunk from 'redux-thunk'
import { applyMiddleware, combineReducers, createStore } from 'redux'

import products from './products/reducer'
import productsModifier from './products/modifier/reducer'
import product from './product/reducer'
import toast from './toast/reducer'

/**
 * FYI
 * ---
 *
 * I use several patterns when defining my Redux store. I usually create two
 * files which correspond to their function: `reducer.js` and `action.js`.
 *
 * ## Action.js
 *
 * I define the action names in a single `types` constant, which is exported
 * as the `action.js` default variable. The `types` is an object that maps
 * the names to a key. This pattern allows me to import the `types` from
 * any `reducer.js` file and easily "catch" the action from said `types`
 * to be handled in that reducer.
 *
 * The `types` is mapped with the following pattern:
 *
 * - The key should sum up the action, e.g. `IS_FETCHING`
 * - The value is the key prefixed with a "namespace",
 * for example: `PRODUCT_IS_FETCHING`
 *
 * The `action.js` file also exports two other variables: the first one
 * is `actions`, which maps available action creators in the file. The other
 * is `dispatchers` which is an object that defines the value for
 * `mapDispatchToState` function of Redux.
 *
 * ### Action creator
 *
 * Speaking of which, my action creators also follow a certain pattern. Each
 * action creator is an object that should contain *exactly* two key:
 * `type` and `payload`. The `type` obviously refers to one of `types` members.
 * The `payload` contains all data needed on the reducer, note that this
 * key can be omitted.
 *
 * I find grouping the whole, well, payload of an action in one key is more
 * readable and easier to manage. Especially when we pass need to pass
 * the data around.
 *
 * The action creator's variable name is based on its `type` value. It is the
 * lower, camel cased version of the `type` string. So if the `type` is
 * 'PRODUCT_IS_FETCHING', the action creator is `productIsFetching`. This
 * naming convention helps with guessing the action that is going
 * to be dispatched.
 *
 * ### Sorting
 *
 * The values of `types` are sorted alphabetically. The action creators
 * variables follow this convention, they are declared alphabetically. It is
 * intended to help with locating each variable or value.
 *
 * ## Reducer.js
 *
 * The reducer is pretty straightforward. It imports necessary `types` variables
 * from `action.js` files. It then uses a `switch` conditional to determine
 * what to do. I am not going into the debate on which one is faster between
 * `switch` and `if-else`. I believe the difference is too marginal to be cared
 * about.
 *
 * ### Reset state
 *
 * Each reducer would also have a `resetState` condition. This is usually
 * triggered upon a `componentWillUnmount()` hook. This condition is only
 * necessary when the component's state should not be persisted throughout
 * the app's lifecycle.
 *
 * Depending on the values of the reducer's `initialState`, a `RESET_STATE`
 * handler could be simply defined as `{...initialState}`, or abstracted
 * to a `resetState()` function when one of the `initialState`'s value
 * is an array or an object.
 *
 */

const app = combineReducers({
  products,
  productsModifier,
  product,
  toast,
})
let store = createStore(app, applyMiddleware(thunk))

export default store
