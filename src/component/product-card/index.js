import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Icon } from 'semantic-ui-react'
import { bool, func, object, string } from 'prop-types'

import ProductImage from './image'
import ProductPreBuy from './pre-buy'
import ProductShare from './share'
import SquareUI from '../square-ui'
import { formatCurrency } from '../../lib'
import './index.css'

/**
 * Actionables on the item.
 * @param {object} item - Product item
 * @param {function} like - Toggle like
 * @param {function} share - Toggle share
 * @param {function} buy - Toggle pre-buy
 */
const CardAction = ({ item, like, share, buy, noLink }) => (
  <div className="ssk product-card action clearfix">
    <div className="ssk float-left">
      <Icon
        className={ item.liked ? 'liked' : ''}
        name={'heart'}
        onClick={like}
      />
      <Icon name={'share alternate'} onClick={share} />
      {
        noLink === true ? null :
        <Link to={`/product/${item.key}`}>
          <Icon name={'info'} />
        </Link>
      }
    </div>
    <div className="ssk float-right">
      <Icon name={'shopping bag'} onClick={buy} />
    </div>
  </div>
)

/**
 * Info detail for the item.
 * @param {number} likes - Like count
 * @param {number} price - Item price
 */
const CardItemInfo = ({ likes, price }) => (
  <div className="ssk product-card info clearfix">
    <div className="ssk float-left">
      <span>
        <strong>{likes}</strong> <span className="fx">sista suka</span>
      </span>
    </div>
    <div className="ssk float-right">
      <strong>Rp {formatCurrency(price)}</strong>
    </div>
  </div>
)

class ProductCard extends Component {
  static propTypes = {
    /** Product item */
    item: object.isRequired,

    /** For when product card is used in product page */
    noLink: bool,

    /** To disable image scrolling */
    noScroll: bool,

    /** Product ID */
    productId: string.isRequired,

    /** Toggle like */
    toggleLike: func.isRequired,
  }

  state = {
    /** Flag to toggle the pre-buy UI */
    showBuyConfirmation: false,

    /** Flag to toggle the share box UI */
    showShareBox: false,
  }

  /**
   * Toggle the buy confirmation UI.
   */
  togglePreBuy() {
    const toggled = ! this.state.showBuyConfirmation
    this.setState({ showBuyConfirmation: toggled })
  }

  /**
   * Toggle the share box UI.
   */
  toggleShareBox() {
    const toggled = ! this.state.showShareBox
    this.setState({ showShareBox: toggled })
  }

  /**
   * Render the component.
   */
  render() {
    const { productId, item, toggleLike, noLink, noScroll } = this.props

    return (
      <div className="ssk product-card wrapper">
        <SquareUI>
          <ProductImage
            images={item.images}
            productId={productId}
            noScroll={noScroll}
          />
          <ProductShare
            item={item}
            isShown={this.state.showShareBox}
            hide={this.toggleShareBox.bind(this)}
          />
          <ProductPreBuy
            item={item}
            isShown={this.state.showBuyConfirmation}
            hide={this.togglePreBuy.bind(this)}
          />
        </SquareUI>

        <div className="ssk product-card meta">
          <div className="ssk product-card inner-meta">
            <CardAction
              item={item}
              like={() => toggleLike(productId)}
              share={() => this.toggleShareBox()}
              buy={() => this.togglePreBuy()}
              noLink={noLink}
            />
            <CardItemInfo likes={item.likes} price={item.price} />
            <p className="ssk product-card name">{item.name}</p>
          </div>
        </div>
      </div>
    )
  }
}

export default ProductCard
