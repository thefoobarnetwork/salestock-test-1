import React, { Component } from 'react'
import { Button, ButtonGroup, Icon } from 'semantic-ui-react'
import { bool, func, object } from 'prop-types'

import { capitalizeWords, reOrderSizeMap } from '../../lib'
import './pre-buy.css'

/**
 * Product is not available.
 * @param {string} name - Product name
 */
const EmptyState = ({ name }) => (
  <div className="ssk product-pre-buy empty-state">
    <p>Maaf Sista, sayang sekali stok {name} masih kosong.</p>
  </div>
)

/**
 * Product is available, show options.
 * @param {object} item - Product item
 * @param {array} sizes - Product sizes
 * @param {function} buy - Buy product
 */
const PreBuyUI = ({ item, sizes, buy }) => {
  const sizeKeys = Object.keys(sizes)

  return (
    <div className="ssk product-pre-buy available">
      <p>
        Sista akan membeli <strong>{item.name}</strong>, silakan
        pilih ukuran yang available!
      </p>

      <div className="ssk product-pre-buy sizes">
        <ButtonGroup fluid basic inverted>{
          sizeKeys.map(key => {
            const size = sizes[key]
            /**
             * FYI
             * ---
             *
             * This is to balance the whitespace of the CTA. If there are
             * more than 2 buttons, it's more probable that we are going
             * to cramp the CTA area. Hence we choose to use the
             * alias (S/M/L/XL/XXL/XXL).
             *
             * On the other hand, when there are less buttons, it's gonna be
             * very sparse. Hence we choose to use the name (small/medium/etc)
             * to make use of those extra whitespaces.
             */
            const name = sizeKeys.length > 2 ?
              size.alias : capitalizeWords(key, '_')

            return (
              <Button key={key} onClick={() => buy(key)}>
                {name} ({size.stock})
              </Button>
            )
          })}</ButtonGroup>
      </div>
    </div>
  )
}

/**
 * Item bought message.
 * @param {string} message - Success message
 */
const ItemBought = ({ message }) => (
  <div className="ssk product-pre-buy bought">
    <Icon name={'checkmark'} size={'huge'} />
    <p>{message}</p>
  </div>
)

class ProductPreBuy extends Component {
  static propTypes = {
    /** Show/hide the pre-buy UI */
    isShown: bool.isRequired,

    /** Product item data */
    item: object.isRequired,

    /** Hide the pre-buy UI */
    hide: func.isRequired,
  }

  state = {
    /** Flag to toggle cart state */
    addedToCart: false,

    /** Available item sizes */
    sizes: [],

    /** Success message for bought item */
    successMessage: '',
  }

  /**
   * Bootstrap the component.
   */
  componentDidMount() {
    this.setAvailableSizes()
  }

  /**
   * Reset necessary inner state before hiding the UI.
   */
  hideUI() {
    this.setState({
      addedToCart: false,
      successMessage: '',
    })
    this.props.hide()
  }

  /**
   * Set data for available sizes.
   */
  setAvailableSizes() {
    const { item } = this.props
    const sizes = reOrderSizeMap(item.size)

    this.setState({ sizes })
  }

  /**
   * Buy the item on said size.
   * @param {integer} index - Size index
   */
  buySize(index) {
    const size = this.state.sizes[index]
    const { item } = this.props

    // item validation
    if (typeof size === 'undefined') return

    this.setState({
      addedToCart: true,
      successMessage: `${item.name} ukuran ${size.alias} ` +
        `sudah ditambahkan ke keranjang belanja Sista!`
    })

    this.countdownHide()
  }

  /**
   * Hide the UI after countdown completes.
   */
  countdownHide() {
    const countdown = window.setTimeout(() => {
      window.clearTimeout(countdown)
      this.hideUI()
    }, 2500)
  }

  /**
   * Render the component.
   */
  render() {
    const { addedToCart, sizes, successMessage } = this.state
    const { isShown, item } = this.props

    return (
      <div className={
        `ssk product-pre-buy wrapper${ isShown ? ' shown' : ''}`
        }>
        <div
          className="ssk product-pre-buy cta-hide"
          onClick={this.hideUI.bind(this)}>
          <Icon name={'close'} size={'large'} />
        </div>

        {
          // show empty state when no stock available
          item.stock < 1 ?
          <EmptyState name={item.name} /> :

          // show success state when item is added to cart
          addedToCart ?
          <ItemBought message={successMessage} /> :

          // otherwise show the pre-buying options
          <PreBuyUI
            item={item}
            sizes={sizes}
            buy={this.buySize.bind(this)}
          />
        }
      </div>
    )
  }
}

export default ProductPreBuy
