import React, { Component } from 'react'
import { Grid, GridColumn, GridRow, Icon, Loader } from 'semantic-ui-react'
import { bool, func, object } from 'prop-types'

import './share.css'

/** List of sharing providers */
const providers = [
  [
    { key: 'fb', name: 'Facebook', icon: 'facebook official' },
    { key: 'tw', name: 'Twitter', icon: 'twitter' },
    { key: 'em', name: 'Email', icon: 'mail' },
  ],
  [
    { key: 'ln', name: 'Line', icon: 'comment outline' },
    { key: 'wa', name: 'WhatsApp', icon: 'whatsapp' },
    { key: 'ms', name: 'SMS', icon: 'mail outline' },
  ],
]

/**
 * The interface to trigger any sharing action.
 * @param {function} share - Share action
 */
const ShareView = ({ share }) => (
  <Grid className="ssk product-share view">{
    providers.map((providerItems, i) => (
      <GridRow key={i} columns={3}>{providerItems.map((provider, j) => (
        <ShareButton
          key={provider.key}
          provider={provider}
          share={() => share(i, j)}
        />
      ))}</GridRow>
    ))
  }</Grid>
)

/**
 * The CTA button for sharing.
 * @param {object} provider - Provider data
 * @param {function} share - Share action
 */
const ShareButton = ({ provider, share }) => (
  <GridColumn className="ssk product-share share-btn">
    <Icon
      name={provider.icon}
      size={'big'}
      onClick={share}
    />
    <p>{provider.name}</p>
  </GridColumn>
)

/**
 * Indicator for sharing simulation.
 */
const SharingIndicator = () => (
  <div className="ssk product-share simulation">
    <Loader active size={'big'} content={'Membagikan produk...'} />
  </div>
)

/**
 * Success state message.
 * @param {string} message - Success message
 */
const SharingSuccess = ({ message }) => (
  <div className="ssk product-share shared">
    <Icon name={'checkmark'} size={'huge'} />
    <p>{message}</p>
  </div>
)

class ProductShare extends Component {
  static propTypes = {
    /** Show/hide the share box UI */
    isShown: bool.isRequired,

    /** Product item data */
    item: object.isRequired,

    /** Hide the share box UI */
    hide: func.isRequired,
  }

  state = {
    /** Flag to simulate sharing */
    isSharing: false,

    /** Flag to toggle the success state */
    showSuccess: false,

    /** Changeable success message */
    successMessage: '',
  }

  /**
   * Reset the inner state before hiding the UI.
   */
  hideUI() {
    this.setState({
      isSharing: false,
      showSuccess: false,
      successMessage: '',
    })
    this.props.hide()
  }

  /**
   * Simulate sharing process.
   * @param {string} key
   */
  simulateSharing(type, key) {
    // simulate an API call
    const simulation = window.setTimeout(() => {
      window.clearTimeout(simulation)
      this.simulateSuccess()
    }, 2500)
    const providerType = providers[type]
    const provider = providerType[key]

    // flag the sharing process
    this.setState({
      isSharing: true,
      successMessage: `Produk sudah dibagi lewat ${provider.name}`,
    })
  }

  /**
   * Show success state after simulation.
   */
  simulateSuccess() {
    // automatically dismiss the view after countdown
    const countdown = window.setTimeout(() => {
      window.clearTimeout(countdown)
      this.hideUI()
    }, 2500)

    this.setState({
      isSharing: false,
      showSuccess: true,
    })
  }

  /**
   * Render the component.
   */
  render() {
    const { isSharing, showSuccess, successMessage } = this.state
    const { isShown, item } = this.props

    return (
      <div className={`ssk product-share wrapper${ isShown ? ' shown' : ''}`}>
        {
          isSharing ? null :
          <div
            className="ssk product-share cta-hide"
            onClick={this.hideUI.bind(this)}>
            <Icon name={'close'} size={'large'} />
          </div>
        }

        <p>
          Dapatkan <strong>Sale Stock Poin</strong> kalo temen Sista
          belanja <strong>{item.name}</strong> lewat tautan yang Sista bagikan!
        </p>

        {
          isSharing || showSuccess ? null :
          <ShareView share={this.simulateSharing.bind(this)} />
        }
        { isSharing ? <SharingIndicator /> : null }
        { showSuccess ? <SharingSuccess message={successMessage} /> : null }
      </div>
    )
  }
}

export default ProductShare
