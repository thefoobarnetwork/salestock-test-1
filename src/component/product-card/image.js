import React, { Component } from 'react'
import Flickity from 'flickity'
import { Image } from 'semantic-ui-react'
import { bool, object, string } from 'prop-types'

import 'flickity/dist/flickity.min.css'
import './image.css'

class ProductImage extends Component {
  /**
   * FYI
   * ---
   *
   * The side-scrolling is powered by Flickity (https://flickity.metafizzy.co/)
   * which is not exactly a free-to-use library. I chose to use this simply
   * because it fits my needs for a proof-of-concept.
   */

  static propTypes = {
    /** List of images to be displayed */
    images: object.isRequired,

    /** Flag to disable horizontal scrolling */
    noScroll: bool,

    /**
     * Identification for when there are
     * multiple `ProductImage` components on the page
     */
    productId: string.isRequired,
  }

  state = {
    /** Unique ID for when the same product is on the same page */
    uniqueId: Date.now(),
  }

  /**
   * Initialize side scrolling on load.
   */
  componentDidMount() {
    // setup side scrolling only when the `noScroll` flag is down
    if ( ! this.props.noScroll) {
      new Flickity(`#img-${this.props.productId + this.state.uniqueId}`, {
        adaptiveHeight: true,
        pageDots: Object.keys(this.props.images).length > 1,
      })
    }
  }

  /**
   * Render the component.
   */
  render() {
    const { images, productId, noScroll } = this.props

    return (
      <div className="ssk product-image">
        <div
          id={`img-${productId + this.state.uniqueId}`}
          className="carousel">{
          Object.keys(images).map((key, i) => {
            if (i > 0 && noScroll) return null

            return (
              <div key={key} className="carousel-cell">
                <Image src={images[key]} fluid />
              </div>
            )
          })
        }</div>
      </div>
    )
  }
}

export default ProductImage
