import React from 'react'
import { Route } from 'react-router-dom'

/**
 * Render route with passable properties.
 * @param {Node} component - The component to be mounted
 */
const PropRoute = ({ component: MountComponent, ...rest }) => (
  <Route {...rest} render={props => (
    <MountComponent {...rest} {...props} />
  )} />
)

export default PropRoute
