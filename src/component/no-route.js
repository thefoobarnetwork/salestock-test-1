import React from 'react'
import { FOF_PATH, HOME_PATH } from '../config'
import { Redirect } from 'react-router-dom'

/**
 * Redirect to not-found handler upon route not exist.
 * @param {object} props - Route properties
 */
const NoRoute = props => (
  <Redirect to={props.location.pathname === '/' ? HOME_PATH : FOF_PATH} />
)

export default NoRoute
