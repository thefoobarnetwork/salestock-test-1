import React from 'react'
import './square-ui.css'

/**
 * Keep the square ratio for UI.
 * @param {node} children - Child element
 */
const SquareUI = ({ children }) => (
  <div className="ssk square-ui ratio-maker">
    <div className="ssk square-ui height-guard">{children}</div>
  </div>
)

export default SquareUI
