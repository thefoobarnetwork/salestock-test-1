import React from 'react'
import { Link } from 'react-router-dom'
import { Icon } from 'semantic-ui-react'

import { HOME_PATH } from '../config'

/**
 * Indicate that the user has been redirected from non-existing URL.
 */
const NotFound = () => (
  <div
    className="ssk not-found"
    style={{
      padding: '28px 14px',
      textAlign: 'center',
    }}>
    <Icon size={'huge'} name={'exclamation'} />
    <p>Oops, tautan Sista tidak bisa kami temukan</p>
    <p><Link to={HOME_PATH}>Kembali ke halaman utama?</Link></p>
  </div>
)

export default NotFound
