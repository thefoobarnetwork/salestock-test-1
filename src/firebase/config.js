/** Configuration to initialize Firebase */
const firebaseConfig = {
  apiKey: "AIzaSyChXCzJIllZTaRTZThD1tDUk-5sS_FrkZM",
  authDomain: "countr-app-p90d.firebaseapp.com",
  databaseURL: "https://countr-app-p90d.firebaseio.com",
  projectId: "countr-app-p90d",
  storageBucket: "countr-app-p90d.appspot.com",
  messagingSenderId: "110830418888"
}

export default firebaseConfig
