import firebase from 'firebase'
import config from './config'

/** The main instance of Firebase app */
firebase.initializeApp(config)

/**
 * FYI
 * ---
 *
 * Below this point are shorthands to quickly access different services
 * and references of Firebase app.
 */

export const fireAuth = firebase.auth
export const fireDB = firebase.database
export const fireStore = firebase.storage

/**
 * FYI
 * ---
 *
 * Below are shorthands for database references.
 */

/**
 * Get reference to database point based on path.
 * @param {string} path - Database path
 * @param {string} extraPath - Additional path
 */
const getDatabaseRef = (path, extraPath) => {
  if (extraPath) path += `/${extraPath}`
  return fireDB().ref(path)
}

/**
 * Get reference to all categories.
 */
export const refCategories = () => getDatabaseRef('categories')

/**
 * Get reference to one category.
 * @param {string} categoryId - Firebase ID
 * @param {string} path - Additional path
 */
export const refCategory = (categoryId, path) => getDatabaseRef(
  `categories/${categoryId}`, path
)

/**
 * Get reference to all tags.
 */
export const refTags = () => getDatabaseRef('tags')

/**
 * Get reference to one tag.
 * @param {string} tagId - Firebase ID
 * @param {string} path - Additional path
 */
export const refTag = (tagId, path) => getDatabaseRef(
  `tags/${tagId}`, path
)

/**
 * Get reference to all users.
 */
export const refUsers = () => getDatabaseRef('users')

/**
 * Get reference to a user.
 * @param {string} uid - Firebase ID
 * @param {string} path - Additional path
 */
export const refUser = (uid, path) => getDatabaseRef(
  `users/${uid}`, path
)

/**
 * Get reference to all products.
 */
export const refProducts = () => getDatabaseRef('products')

/**
 * Get reference to a product.
 * @param {string} productId - Firebase ID
 * @param {string} path - Additional path
 */
export const refProduct = (productId, path) => getDatabaseRef(
  `products/${productId}`, path
)

/**
 * Get reference to all shoes.
 */
export const refAllShoes = () => getDatabaseRef('shoes')

/**
 * Get reference to a pair of shoes.
 * @param {string} shoesId - Firebase ID
 * @param {string} path - Additional path
 */
export const refShoes = (shoesId, path) => getDatabaseRef(
  `shoes/${shoesId}`, path
)

/**
 * Get reference to all accessories.
 */
export const refAccessories = () => getDatabaseRef('accessories')

/**
 * Get reference to an accessory.
 * @param {string} accessoryId - Firebase ID
 * @param {string} path - Additional path
 */
export const refAccessory = (accessoryId, path) => getDatabaseRef(
  `accessories/${accessoryId}`, path
)
