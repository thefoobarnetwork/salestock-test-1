/**
 * FYI
 * ---
 *
 * I decided to abstract out Firebase call into API services. This pattern
 * is great for app's testability. By separating the actual API call from
 * the component, and using interface in its stead, we can easily swap the API
 * call with whatever things we need.
 *
 * For example, during tests, it's better to fake the API call due to
 * network latency. We can easily use a `timeout` based API service that
 * returns stubs for each API call.
 *
 * Similarly, if we decide to swap the API endpoints, or maybe the library,
 * we can easily do so. For example, right now the app uses Firebase library.
 * But we can easily switch to Axios + whatever-API-service if we deem so.
 *
 * The only constraints are that (1) the interfaces should be the same in all
 * situations, and (2) the response should mimic the actual payload.
 * E.g. the stub for DressAPI should have the same names for its
 * callable methods as the DressAPI, and returns similar (or exactly the same)
 * payload as DressAPI.
 */

export { default as AccessoryAPI } from './accessory'
export { default as DressAPI } from './dress'
export { default as ShoesAPI } from './shoes'
export { default as SubDressAPI } from './sub-dress'

