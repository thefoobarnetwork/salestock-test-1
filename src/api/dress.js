import { refProduct, refProducts } from '../firebase'

const DressAPI = {
  /**
   * Fetch items for initial load.
   * @param {number} itemNumbers - Items to load
   */
  fetchInitialProducts: itemNumbers => refProducts().orderByKey()
    .limitToLast(itemNumbers)
    .once('value'),

  /**
   * Fetch more items, pad the numbers of items to load by one.
   * @param {string} refKey - Reference Firebase ID
   * @param {number} itemNumbers - Items to load
   */
  fetchMoreProducts: (refKey, itemNumbers) => refProducts().orderByKey()
    .endAt(refKey)
    .limitToLast(itemNumbers + 1)
    .once('value'),

  /**
   * Fetch an item.
   * @param {string} refKey - Reference Firebase ID
   */
  fetchProduct: refKey => refProduct(refKey).once('value'),

  /**
   * Fetch related product data based on tag.
   * @param {string} tagId - Reference Firebase ID
   */
  fetchRelatedProduct: tagId => refProducts().orderByChild('tags')
    .equalTo(tagId)
    .once('value'),
}

export default DressAPI
