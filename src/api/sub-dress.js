import { refProducts } from '../firebase'

// Hard code the data for sub-category for demo purpose
const acceptableSubCategory = [
  'maxi-dress',
  'midi-dress',
  'mini-dress',
]
const subCategoryIds = {
  'maxi-dress': '-L7Yr0uhwlgPK61emyen',
  'midi-dress': '-L7Yr0ufMHW_SP7KsUeZ',
  'mini-dress': '-L7Yr0udMPV93MDzE5EM',
}

/**
 * Interface for fetching sub-category of Dress.
 * @param {string} sub - Sub Category
 */
function SubDressAPI(sub) {
  let productList = {}
  let productIds = []
  // flags for the products fetching
  let flag = {
    // wait for completing pre-fetch
    preFetch: false,
    // issue error due to pre-fetch
    issueError: false,
  }
  let tagId

  /**
   * Wait for fetch to complete before creating API response.
   * @param {function} reject - Reject promise
   * @param {function} callback - API action
   */
  const waitForFetch = (reject, callback) => {
    const timer = window.setInterval(() => {
      if ( ! flag.preFetch) return

      window.clearInterval(timer)
      // throw error when flagged like so
      if (flag.issueError) {
        return reject({
          code: null,
          message: 'Pre-fetch data failed',
        })
      }

      // otherwise, run the callback
      callback()
    }, 100)
  }

  // return empty object when sub does not exist
  if (acceptableSubCategory.indexOf(sub) < 0) return {}
  // otherwise, set the tag ID to use
  tagId = subCategoryIds[sub]

  // run pre-fetch
  refProducts().orderByChild('tags').equalTo(tagId).once('value').then(
    /**
     * Set data from pre-fetch for reference.
     * @param {object} snapshot - Firebase snapshot
     */
    snapshot => {
      const value = snapshot.val() || {}

      productIds = Object.keys(value).sort().reverse()
      productIds.forEach(id => productList[id] = value[id])

      // raise flag as necessary
      flag.preFetch = true
      flag.issueError = false
    }
  ).catch(
    /**
     * Just raise flag when error happens.
     */
    ({ code, message}) => {
      flag.preFetch = true
      flag.issueError = true
      // log for development
      console.log('ERROR CODE', code)
      console.log('ERROR MESSAGE', message)
    }
  )

  return {
    /**
     * Fetch items for initial load.
     * @param {number} itemNumbers - Items to load
     */
    fetchInitialProducts: itemNumbers => {
      const fetch = new Promise((resolve, reject) => {
        const initialProducts = () => {
          let response = {}

          productIds.forEach((id, i) => {
            if (i >= itemNumbers) return
            response[id] = productList[id]
          })

          // mimic Firebase's payload
          resolve({
            val: () => response
          })
        }

        waitForFetch(reject, initialProducts)
      })

      return fetch
    },

    /**
     * Fetch more items, pad the numbers of items to load by one.
     * @param {string} refKey - Reference Firebase ID
     * @param {number} itemNumbers - Items to load
     */
    fetchMoreProducts: (refKey, itemNumbers) => {
      const fetch = new Promise((resolve, reject) => {
        const moreProducts = () => {
          let padIndex = productIds.indexOf(refKey)
          let maxIndex = padIndex + itemNumbers
          let response = {}

          for (padIndex; padIndex <= maxIndex; padIndex++) {
            let id = productIds[padIndex]

            if (typeof id === 'undefined') break
            response[id] = productList[id]
          }

          // mimic Firebase's payload
          resolve({
            val: () => response
          })
        }

        waitForFetch(reject, moreProducts)
      })

      return fetch
    },
  }
}

export default SubDressAPI
