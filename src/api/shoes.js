import { refAllShoes } from '../firebase'

const ShoesAPI = {
  /**
   * Fetch items for initial load.
   * @param {number} itemNumbers - Items to load
   */
  fetchInitialProducts: itemNumbers => refAllShoes().orderByKey()
    .limitToLast(itemNumbers)
    .once('value'),

  /**
   * Fetch more items, pad the numbers of items to load by one.
   * @param {string} refKey - Reference Firebase ID
   * @param {number} itemNumbers - Items to load
   */
  fetchMoreProducts: (refKey, itemNumbers) => refAllShoes().orderByKey()
    .endAt(refKey)
    .limitToLast(itemNumbers + 1)
    .once('value'),
}

export default ShoesAPI
