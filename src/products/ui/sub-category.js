import React from 'react'
import { Link } from 'react-router-dom'
import { Grid, GridColumn, GridRow } from 'semantic-ui-react'

import store from '../../store'
import { SquareUI } from '../../component'
import { actions } from '../action'

/**
 * Reusable dispatcher for product reload.
 */
const _dispatchReloadProducts = () => {
  store.dispatch(actions.productsReloadState())
}

/**
 * FYI
 * ---
 *
 * Since this is just a proof of concept, I decide to hard code the menu
 * options for sub category.
 */

/**
 * Sub category selection.
 */
const SubCategory = () => {
  const subs = [{
    name: 'Mini Dress',
    path: '/category/dress/sub/mini-dress',
  }, {
    name: 'Midi Dress',
    path: '/category/dress/sub/midi-dress',
  }, {
    name: 'Maxi Dress',
    path: '/category/dress/sub/maxi-dress',
  }]

  return (
    <div className="ssk product-sub wrapper">
      <p className="ssk product-sub title"><strong>Dress</strong></p>
      <p className="ssk product-sub meta">
        Koleksi dress cantik sesuai gaya Sista dimanapun
      </p>

      <Grid>
        <GridRow columns={subs.length}>{subs.map((item, i) => (
          <SubCategoryOption key={i} item={item} />
        ))}</GridRow>
      </Grid>
    </div>
  )
}

/**
 * Option action for the sub category.
 * @param {object} item - Sub category
 */
const SubCategoryOption = ({ item }) => (
  <GridColumn>
    <SquareUI>
      <div className="ssk product-sub option">
        <Link to={item.path} onClick={_dispatchReloadProducts}>
          <p>{item.name}</p>
        </Link>
      </div>
    </SquareUI>
  </GridColumn>
)

/**
 * Back to category view.
 */
const ToCategory = () => (
  <div className="ssk product-sub wrapper">
    <p className="ssk product-sub title">
      <strong>
        <Link
          to={'/category/dress'}
          onClick={_dispatchReloadProducts}>
          Back to dress
        </Link>
      </strong>
    </p>
    <p className="ssk product-sub meta">
      Lebih banyak lagi koleksi dress cantik sesuai gaya Sista dimanapun
    </p>
  </div>
)

export { SubCategory, ToCategory }
