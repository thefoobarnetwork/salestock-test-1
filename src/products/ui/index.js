import React from 'react'
import { Loader } from 'semantic-ui-react'
import { bool, func, object } from 'prop-types'

import ProductsModifier from '../modifier'
import { ProductCard } from '../../component'
import { SubCategory, ToCategory } from './sub-category'

/**
 * Loading indicator for products page.
 */
const LoadingIndicator = () => (
  <div className="ssk products-load">
    <Loader active content={'Tunggu sebentar...'} />
  </div>
)

/**
 * Indicator for all data loaded.
 */
const LoadComplete = () => (
  <div className="ssk products-loaded">
    <p>Tidak ada produk lain lagi</p>
  </div>
)

/**
 * Render the UI for products.
 * @param {boolean} completed - All data loaded
 * @param {boolean} isFetching - Fetching flag
 * @param {boolean} isSubCategory - Detail view
 * @param {object} items - Product items
 */
const ProductsUI = ({
  completed, isFetching, isSubCategory, items, ...actions
}) => {
  const keys = Object.keys(items)

  return (
    <div className="ssk products-ui">
      { keys.length > 0 ? <ProductsModifier /> : null}
      { keys.length > 0 && ! isSubCategory ? <SubCategory /> : null }
      { isSubCategory ? <ToCategory /> : null }

      {keys.map(key => (
        <ProductCard
          key={key}
          item={items[key]}
          productId={key}
          {...actions}
        />
      ))}

      { isFetching ? <LoadingIndicator /> : null }
      { completed ? <LoadComplete /> : null }
    </div>
  )
}

ProductsUI.propTypes = {
  /** All data loaded */
  completed: bool.isRequired,

  /** Fetching flag */
  isFetching: bool.isRequired,

  /** Detail view flag */
  isSubCategory: bool.isRequired,

  /** Product items */
  items: object.isRequired,

  /** Toggle like */
  toggleLike: func.isRequired,
}

export default ProductsUI
