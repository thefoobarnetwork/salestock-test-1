import React, { Component } from 'react'
import { Button, Checkbox, Select } from 'semantic-ui-react'
import { func } from 'prop-types'
import { filterByPrice, filterBySize, sortOptions } from './lib'

/**
 * Filters CTA trigger.
 * @param {boolean} isShown - Filters shown
 * @param {function} show - Show filters
 * @param {function} hide - Hide filters
 */
const FilterCTA = ({ isShown, show, hide }) => (
  <div
    className="filter-cta"
    onClick={() => isShown ? hide() : show()}>
    <span>Harga</span>
    <span>Ukuran</span>
  </div>
)

/**
 * Display the filter list.
 * @param {boolean} isShown - Filters shown
 * @param {object} values - Filter reference
 * @param {function} hide - Hide filters
 * @param {function} toggle - Select toggle
 * @param {function} apply - Apply filters
 */
const FiltersList = ({ isShown, values, hide, toggle, apply }) => {
  const itemsProps = { values, toggle }

  return (
    <div className={
      `ssk products-modifier filters${isShown ? ' shown' : ''}`
      }>
      <div className="ssk products-modifier filters-inner clearfix">
        <div className="filter-wrapper title">
          <p>Rentang Harga</p>
        </div>
        <FilterItems filters={filterByPrice} {...itemsProps} />

        <div className="filter-wrapper title">
          <p>Ukuran Pakaian</p>
        </div>
        <FilterItems filters={filterBySize} {...itemsProps} />

        <div className="filter-wrapper cta">
          <Button basic className="cx" onClick={hide}>Cancel</Button>
          <Button basic onClick={apply}>Apply</Button>
        </div>
      </div>
    </div>
  )
}

/**
 * Display the filter items UI.
 * @param {object} filters - Filter items
 * @param {object} values - Filter reference
 * @param {function} toggle - Select toggle
 */
const FilterItems = ({ filters, values, toggle }) => (
  Object.keys(filters).map(key => (
    <div key={key} className="filter-wrapper item">
      <Checkbox
        label={filters[key].alias}
        checked={values[key]}
        onClick={() => toggle(key)}
      />
    </div>
  ))
)

class ProductsModifierUI extends Component {
  /**
   * FYI
   * ---
   *
   * Why do we maintain so many inner states for this component?
   *
   * The reason is simply to minimize the number of dispatches. Every time a
   * dispatch happens, the whole app is updated (since they subscribe to
   * the state). Even though React is smart enough to say which component to
   * re-paint, it still runs some operation on DOMs, which is always
   * an expensive thing to do.
   *
   * So what we are doing here is: wait until user explicitly say "OK"
   * before actually dispatching the filter actions.
   *
   * This requires us to maintain many references, as well as inner states,
   * since we need to separate the states from Redux's actual stores.
   * Hence why this component has so many inner state management happening.
   */

  static propTypes = {
    /** Persist filter selection to store */
    setFilters: func.isRequired,
  }

  state = {
    /** Inner reference for filter items selection */
    filterRef: {},

    /** Flag to show/hide filters list */
    showFilters: false,
  }

  /**
   * Bootstrap component.
   */
  componentDidMount() {
    this.props.showAllProducts()
    this.syncSelectedFilterRef()
  }

  /**
   * Clean-ups before unmounting.
   */
  componentWillUnmount() {
    this.props.clearState()
  }

  /**
   * Setup a reference object for filter state.
   */
  syncSelectedFilterRef() {
    let filterArr = [filterByPrice, filterBySize]
    let filterRef = {}

    filterArr.forEach(
      filters => Object.keys(filters).forEach(
        key => filterRef[key] = filters[key].selected
      )
    )
    this.setState({ filterRef })
  }

  /**
   * Show filters list.
   */
  showFilters() {
    this.setState({ showFilters: true })
  }

  /**
   * Hide filters list.
   */
  hideFilters() {
    // `hideFilters()` exclusively hides the filter list
    // and resync the reference with the store's state
    this.setState({ showFilters: false })
    this.syncSelectedFilterRef()
  }

  /**
   * Toggle filter selection state.
   * @param {string} key - Filter key
   */
  toggleSelectFilter(key) {
    let { filterRef } = this.state

    filterRef[key] = ! filterRef[key]
    this.setState({ filterRef })
  }

  /**
   * Persist filters to store using inner state `filterRef`.
   */
  persistFilters() {
    this.props.setFilters(this.state.filterRef)
    // we can't use `this.hideFilters()` since it would reset the
    // `filterRef` to previous state, which we don't want to in this case
    this.setState({ showFilters: false })
  }

  /**
   * Render the component.
   */
  render() {
    const { filterRef, showFilters } = this.state

    return (
      <div className="ssk products-modifier wrapper">
        <div className="ssk products-modifier cta clearfix">
          <div className="cta-wrapper filter">
            <FilterCTA
              isShown={showFilters}
              show={this.showFilters.bind(this)}
              hide={this.hideFilters.bind(this)}
            />
          </div>

          <div className="cta-wrapper sort">
            <Select fluid options={sortOptions} />
          </div>
        </div>

        <FiltersList
          isShown={showFilters}
          values={filterRef}
          hide={this.hideFilters.bind(this)}
          toggle={this.toggleSelectFilter.bind(this)}
          apply={this.persistFilters.bind(this)}
        />
      </div>
    )
  }
}

export default ProductsModifierUI
