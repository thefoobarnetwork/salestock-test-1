import ProductsModifierUI from './ui'
import { connect } from 'react-redux'
import { dispatchers } from './action'
import './index.css'

/**
 * Map the state to component's properties.
 * @param {object} state - App state
 */
const mapState = state => ({...state.productsModifier})

export default connect(mapState, dispatchers)(ProductsModifierUI)
