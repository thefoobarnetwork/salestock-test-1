import { filterByPrice, filterBySize } from './lib'

/** List of action names */
const types = {
  RESET_STATE: 'MODIFIER_RESET_STATE',
  SET_FILTERS: 'MODIFIER_SET_FILTERS',
  SHOW_ALL: 'MODIFIER_SHOW_ALL',
}

/**
 * Reset the store's state.
 */
const modifierResetState = () => ({ type: types.RESET_STATE })

/**
 * Activate filters by passing the key names.
 * @param {array} filters - Filter keys
 * @param {object} products - Product items
 */
const modifierSetFilters = (filters = [], products = {}) => ({
  type: types.SET_FILTERS,
  payload: { filters, products }
})

/**
 * Unset all filters and sorts.
 * @param {object} products - Product items
 */
const modifierShowAll = (products = {}) => ({
  type: types.SHOW_ALL,
  payload: { products },
})

/** List of action creators */
const actions = {
  modifierResetState,
}

/** List of callable dispatchers */
const dispatchers = {
  /**
   * Clear state when needed.
   * @param {function} disptach - Dispatch action
   */
  clearState: () => dispatch => {
    const filters = [filterByPrice, filterBySize]

    // reset the reference object library
    filters.forEach(
      list => Object.keys(list).forEach(
        key => list[key].selected = false
      )
    )

    dispatch(modifierResetState())
  },

  /**
   * Show all items at first.
   * @param {function} disptach - Dispatch action
   * @param {function} getState - Get app state
   */
  showAllProducts: () => (dispatch, getState) => {
    const { items } = getState().products
    dispatch(modifierShowAll(items))
  },

  /**
   * Select active filters based on ref object `filters`.
   * @param {object} filterRef - Reference
   * @param {function} disptach - Dispatch action
   * @param {function} getState - Get app state
   */
  setFilters: filterRef => (dispatch, getState) => {
    const { items } = getState().products
    let filters = Object.keys(filterRef)
      .filter(key => filterRef[key])
      .map(key => key)

    dispatch(modifierSetFilters(filters, items))
  },
}

export default types
export { actions, dispatchers }
