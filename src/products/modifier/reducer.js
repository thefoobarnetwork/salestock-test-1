import types from './action'
// import productsTypes from '../action'
import { filterByPrice, filterBySize, sortsList } from './lib'

const initialState = {
  /** Active filters */
  filters: [],

  /** IDs of products to show */
  productsToShow: [],

  /** Type of sorting being selected */
  sort: false,
}

/**
 * Reducer for products modifier.
 * @param {object} state - Store state
 * @param {object} action - Action dispatched
 */
function productsModifier(
  state = initialState,
  action = { type: 'FOO', payload: {} }
) {
  const { type, payload } = action

  switch (type) {
    case types.RESET_STATE: return resetState()
    case types.SET_FILTERS: return filterAndSortProducts(state, payload)
    case types.SHOW_ALL: return showAll(state, payload)
    default: return state
  }
}

/**
 * Modify the list of products to be shown.
 * @param {object} state - App state
 * @param {object} payload - Action payload
 */
function filterAndSortProducts(state, payload) {
  const { products, ...flags } = payload
  // merge the flags to the new state
  let newState = { ...state, ...flags }
  // prepare the new products map
  let toShow = []
  // prepare filters to be ran
  let filtersToRun = []

  newState.filters.forEach(key => {
    const price = filterByPrice[key]
    const size = filterBySize[key]

    if (typeof price !== 'undefined') {
      filtersToRun.push(price)
      price.selected = ! price.selected
    } else if (typeof size !== 'undefined') {
      filtersToRun.push(size)
      size.selected = ! size.selected
    }
  })

  Object.keys(products).forEach(key => {
    let product = products[key]

    for (let i = 0; i < filtersToRun.length; i++) {
      if ( ! filtersToRun[i].validate(product)) return
    }
    toShow.push(product)
  })

  if (sortsList[newState.sort]) {
    toShow = sortsList[newState.sort].validate(toShow)
  }

  newState.productsToShow = toShow
  return newState
}

/**
 * Reset the store's state.
 */
function resetState() {
  return {
    filters: [],
    productsToShow: [],
    sort: false,
  }
}

/**
 * Unset sorts and filters, show all products.
 * @param {object} state - App state
 * @param {object} payload - Action payload
 */
function showAll(state, payload) {
  let productsToShow = Object.keys(payload.products)

  return {
    filters: [],
    productsToShow,
    sort: false,
  }
}

export default productsModifier
