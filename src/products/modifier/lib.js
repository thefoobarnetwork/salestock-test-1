/**
 * Validate size to be filtered.
 * @param {object} sizes - Item sizes
 * @param {string} size - Filter size
 */
const _sizeValidation = (sizes, size) =>
  typeof sizes[size] !== 'undefined' && sizes[size].available

/** Filter by item's price */
const filterByPrice = {
  /** Price up to 50k */
  lessFifty: {
    alias: 'Di bawah 50 ribu',
    selected: false,
    validate: item => item.price <= 50000,
  },

  /** Price is between, inclusive, (50k + 1) to 100k */
  fiftyToHundred: {
    alias: '50 - 100 ribu',
    selected: false,
    validate: item => item.price > 50000 && item.price <= 100000,
  },

  /** Price is between , inclusive, (100k + 1) to 150k */
  hundredToOneFifty: {
    alias: '100 - 150 ribu',
    selected: false,
    validate: item => item.price > 100000 && item.price <= 150000,
  },

  /** Price is more than 150k */
  moreOneFifty: {
    alias: 'Lebih dari 150 ribu',
    selected: false,
    validate: item => item.price > 150000,
  },
}

/** Filter by item's size */
const filterBySize = {
  /** Size is extra small and available */
  extra_small: {
    alias: 'XS',
    selected: false,
    validate: item => _sizeValidation(item.size, 'extra_small'),
  },

  /** Size is small and available */
  small: {
    alias: 'S',
    selected: false,
    validate: item => _sizeValidation(item.size, 'small'),
  },

  /** Size is medium and available */
  medium: {
    alias: 'M',
    selected: false,
    validate: item => _sizeValidation(item.size, 'medium'),
  },

  /** Size is large and available */
  large: {
    alias: 'L',
    selected: false,
    validate: item => _sizeValidation(item.size, 'large'),
  },

  /** Size is extra large and available */
  extra_large: {
    alias: 'XL',
    selected: false,
    validate: item => _sizeValidation(item.size, 'extra_large'),
  },

  /** Size is double extra large and available */
  double_extra_large: {
    alias: 'XXL',
    selected: false,
    validate: item => _sizeValidation(item.size, 'double_extra_large'),
  },

  /** Size is triple extra large and available */
  triple_extra_large: {
    alias: 'XXXL',
    selected: false,
    validate: item => _sizeValidation(item.size, 'triple_extra_large'),
  },
}

/**
 * List of filters available for the app.
 */
const sortsList = {
  /** Sort items by `likes` */
  byPopularity: {
    alias: 'Terpopuler',
    validate: items => Object.keys(items).sort(
      (a, b) => items[b].likes - items[a].likes
    ),
  },

  /** Sort items by lowest `price` */
  byLowPrice: {
    alias: 'Termurah',
    validate: items => Object.keys(items).sort(
      (a, b) => items[a].price - items[b].price
    ),
  },

  /** Sort items by highest `price` */
  byHighPrice: {
    alias: 'Termahal',
    validate: items => Object.keys(items).sort(
      (a, b) => items[b].price - items[a].price
    ),
  },
}

/** Filter options for Select component */
const sortOptions = [
  { key: 'foo', value: false, text: 'Urutkan' },
  { key: 'byPopularity', value: 'byPopularity', text: sortsList.byPopularity.alias },
  { key: 'byLowPrice', value: 'byLowPrice', text: sortsList.byLowPrice.alias },
  { key: 'byHighPrice', value: 'byHighPrice', text: sortsList.byHighPrice.alias },
]

export {
  filterByPrice,
  filterBySize,
  sortOptions,
  sortsList,
}
