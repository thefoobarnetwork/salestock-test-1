import types from './action'

const initialState = {
  /** Flag that all products have been loaded */
  allItemsLoaded: false,

  /** API service to interface */
  apiService: null,

  /** Debouncer flag to avoid sending out redundant requests */
  isFetching: false,

  /** Number of items to load on each fetch */
  itemToLoad: 3,

  /** List of products available */
  items: {},

  /** Oldest key fetched thus far */
  oldestKey: '',
}

/**
 * Reducer for products page.
 * @param {object} state - Store state
 * @param {object} action - Action dispatched
 */
function products(
  state = initialState,
  action = { type: 'FOO', payload: {} }
) {
  const { type, payload } = action

  switch (type) {
    case types.ALL_LOADED:
      return { ...state, allItemsLoaded: true, isFetching: false }
    case types.APPEND_PRODUCTS: return appendProducts(state, payload)
    case types.IS_FETCHED: return { ...state, isFetching: false }
    case types.IS_FETCHING: return { ...state, isFetching: true }
    case types.PERSIST_API_SERVICE:
      return { ...state, apiService: payload.apiService }
    case types.RELOAD_STATE:
      return { ...state, allItemsLoaded: false, items: {}, oldestKey: '' }
    case types.RESET_STATE: return resetState()
    case types.SET_OLDEST_KEY: return { ...state, oldestKey: payload.oldestKey }
    case types.TOGGLE_ITEM_LIKE: return toggleItemLike(state, payload)
    default: return state
  }
}

/**
 * Append loaded products to store.
 * @param {object} state - Store state
 * @param {object} payload - Action payload
 */
function appendProducts(state, payload) {
  const { keys, value } = payload
  let items = {...state.items}

  keys.forEach(key => items[key] = { key, ...value[key] })
  return { ...state, items }
}

/**
 * Reset the store's state.
 */
function resetState() {
  return {
    allItemsLoaded: false,
    apiService: null,
    isFetching: false,
    itemToLoad: 3,
    items: {},
    oldestKey: '',
  }
}

/**
 * Toggle the `liked` state of an item.
 * @param {object} state - Store state
 * @param {object} payload - Action payload
 */
function toggleItemLike(state, payload) {
  let items = {...state.items}
  let item = items[payload.key]

  if ( ! item.liked) item.liked = true
  else item.liked = false

  return { ...state, items }
}

export default products
