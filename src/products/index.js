import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { bool, object } from 'prop-types'

import ProductsUI from './ui'
import * as API from '../api'
import { FOF_PATH } from '../config'
import { capitalizeWords } from '../lib'
import { dispatchers } from './action'
import './index.css'

/**
 * Map the state to component's props.
 * @param {object} state - Store state
 * @param {object} ownProps - Mount component props
 */
const mapState = (state, ownProps) => {
  let { category, tag } = ownProps.match.params
  let apiService = API[`${capitalizeWords(category)}API`] || {}

  // swap the API service to use the sub-category's
  // when the user is in the sub-category route
  if (tag) {
    apiService = API.SubDressAPI(tag.toLowerCase()) || {}
  }

  return {
    ...state.products,
    isSubCategory: ownProps.isSubCategory,
    apiService,
  }
}

class ProductsPage extends Component {
  /**
   * FYI
   * ---
   *
   * The inifinite scrolling load strategy is inspired by
   * this Hackernoon tutorial:
   *
   * https://hackernoon.com/infinite-scrolling-in-firebase-e28bfbc53578
   */

  static propTypes = {
    /** API service to interface with */
    apiService: object.isRequired,

    /** Flag a detail view */
    isSubCategory: bool.isRequired,
  }

  state = {
    /** Flag for when improper category/tag param is passed */
    apiFailure: false,
  }

  /**
   * FYI
   * ---
   *
   * This variable is very important. It helps to avoid entanglements between
   * JavaScript's eventListener and React's `bind()` method.
   *
   * In short, it acts as a reference to the bound method for eventListener.
   *
   * https://gist.github.com/Restuta/e400a555ba24daa396cc
   */
  _trackScroll = null

  /**
   * Bootstrap the component.
   */
  componentDidMount() {
    // flag API service to run
    this.runApiService()

    // bind eventListener
    this._trackScroll = this.trackScrolling.bind(this)
    document.addEventListener('scroll', this._trackScroll)
  }

  /**
   * Clean ups before component unmounted.
   */
  componentWillUnmount() {
    document.removeEventListener('scroll', this._trackScroll)
    this.props.clearState()
  }

  /**
   * Re-bootstrap the component where necessary during path change.
   * @param {object} prevProps - Previous props
   */
  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      this.runApiService()
    }
  }

  /**
   * Run API service setup.
   */
  runApiService() {
    const { apiService } = this.props

    /**
     * FYI
     * ---
     *
     * This setup is abstracted to a method because the same process should
     * be re-run when the route changes within the `category` path. It just
     * makes thing a lot easier.
     */

    // raise flag when service is not found
    if (Object.keys(apiService).length < 1) {
      this.setState({ apiFailure: true })
      return
    }

    // set API service interface
    this.props.setApiService(apiService)
    // begin first API call
    this.props.initialFetch()
  }

  /**
   * Track user scrolls for inifinity scroll feature.
   */
  trackScrolling() {
    const app = document.getElementById('ssk-app')

    // fetch more data when:
    if (
      // the scroll has hit bottom
      (Math.floor(app.getBoundingClientRect().bottom) <= window.innerHeight) &&
      // and there is no other fetch ongoing
      ! this.props.isFetching &&
      // and there is (possibly) still more data to be loaded
      ! this.props.allItemsLoaded
    ) {
      this.props.scrollFetch()
    }
  }

  /**
   * Render seeding status.
   */
  render() {
    const { allItemsLoaded, ...rest } = this.props

    // Redirect to other path if API failure flag is raised
    if (this.state.apiFailure) {
      /**
       * FYI
       * ---
       *
       * It is necessary to redirect to a different path than the ones
       * that the component is currently in. This is so that the redirection
       * would also trigger a re-render; refreshing the component and
       * re-running the bootstrap.
       *
       * Otherwise, the page would be empty since the component has passed
       * the `componentDidMount()` lifecycle and not being bootstrapped
       * correctly due to no re-render happening.
       *
       * In this case, we redirect to the 404 page to bring the user back
       * to the main product page.
       */
      return <Redirect to={FOF_PATH} />
    }

    // console.log(this.props.items)
    return (
      <ProductsUI completed={allItemsLoaded} {...rest} />
    )
  }
}

export default connect(mapState, dispatchers)(ProductsPage)
