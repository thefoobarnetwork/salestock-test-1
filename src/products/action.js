import { actions as toastActions } from '../toast/action'

/** List of action names */
const types = {
  ALL_LOADED: 'PRODUCTS_ALL_LOADED',
  APPEND_PRODUCTS: 'PRODUCTS_APPEND_PRODUCTS',
  IS_FETCHED: 'PRODUCTS_IS_FETCHED',
  IS_FETCHING: 'PRODUCTS_IS_FETCHING',
  PERSIST_API_SERVICE: 'PRODUCTS_PERSIST_API_SERVICE',
  RELOAD_STATE: 'PRODUCTS_RELOAD_STATE',
  RESET_STATE: 'PRODUCTS_RESET_STATE',
  SET_OLDEST_KEY: 'PRODUCTS_SET_OLDEST_KEY',
  TOGGLE_ITEM_LIKE: 'PRODUCTS_TOGGLE_ITEM_LIKE',
}

/**
 * All products have been loaded.
 */
const productsAllLoaded = () => ({ type: types.ALL_LOADED })

/**
 * Append products to the store.
 * @param {array} keys - Item keys
 * @param {object} value - Item values
 */
const productsAppendProducts = (keys = [], value = {}) => ({
  type: types.APPEND_PRODUCTS,
  payload: { keys, value }
})

/**
 * Fetching completed.
 */
const productsIsFetched = () => ({ type: types.IS_FETCHED })

/**
 * Fetching ongoing.
 */
const productsIsFetching = () => ({ type: types.IS_FETCHING })

/**
 * Persist the API service to state for ease of access.
 * @param {object} apiService - API interface
 */
const productsPersistApiService = (apiService = {}) => ({
  type: types.PERSIST_API_SERVICE,
  payload: { apiService }
})

/**
 * Force to reload state partially.
 */
const productsReloadState = () => ({ type: types.RELOAD_STATE })

/**
 * Reset the store's state.
 */
const productsResetState = () => ({ type: types.RESET_STATE })

/**
 * Set reference to the latest ID.
 * @param {string} oldestKey - Firebase ID
 */
const productsSetOldestKey = (oldestKey = '') => ({
  type: types.SET_OLDEST_KEY,
  payload: { oldestKey }
})

/**
 * Toggle an item's like.
 * @param {string} key - Firebase ID
 */
const productsToggleItemLike = (key = '') => ({
  type: types.TOGGLE_ITEM_LIKE,
  payload: { key }
})

/** List of action creators */
const actions = {
  productsReloadState,
}

/** List of callable dispatchers */
const dispatchers = {
  /**
   * Trigger store's reset state.
   */
  clearState: () => productsResetState(),

  /**
   * Trigger API service persisting.
   */
  setApiService: apiService => productsPersistApiService(apiService),

  /**
   * Start initial fetching.
   * @param {function} dispatch - Dispatch action
   * @param {function} getState - Get state
   */
  initialFetch: () => (dispatch, getState) => {
    const { apiService, allItemsLoaded, itemToLoad } = getState().products

    // flag fetching
    dispatch(productsIsFetching())

    apiService.fetchInitialProducts(itemToLoad).then(
      /**
       * Persist the initial fetch to the component.
       * @param {object} snapshot - Firebase snapshot
       */
      snapshot => {
        // snapshot.val() might return null,
        // we set a default value of an empty object
        const value = snapshot.val() || {}
        // reverse keys to be chronological order, latest product first
        const keys = Object.keys(value).sort().reverse()
        // set the reference key
        const oldestKey = keys[keys.length - 1]

        // abort when all data has been loaded
        if (allItemsLoaded) return true

        // otherwise, set the data
        dispatch(productsAppendProducts(keys, value))
        dispatch(productsSetOldestKey(oldestKey))
        dispatch(productsIsFetched())
        // check if all data loaded
        if (keys.length < itemToLoad) dispatch(productsAllLoaded())
      }
    ).catch(
      /**
       * Use toast to notify about the error.
       * @param {string} code - Error code
       * @param {string} message - Error message
       */
      ({ code, message }) => {
        dispatch(productsIsFetched())
        dispatch(toastActions.toastErrorMessage(message, code))
      }
    )
  },

  /**
   * Start infinity scroll fetching.
   * @param {function} dispatch - Dispatch action
   * @param {function} getState - Get state
   */
  scrollFetch: () => (dispatch, getState) => {
    const {
      apiService, allItemsLoaded, itemToLoad, oldestKey
    } = getState().products

    // safety for when the component dismounted
    // it triggers the scroll tracker since the div size changes
    if ( ! apiService) return

    // flag fetching
    dispatch(productsIsFetching())

    apiService.fetchMoreProducts(oldestKey, itemToLoad).then(
      /**
       * Persist the scroll fetch to the component.
       * @param {object} snapshot - Firebase snapshot
       */
      snapshot => {
        // See notes about defaulting value to empty object
        // at `persistInitialFetch()`
        const value = snapshot.val() || {}
        // the `.slice(1)` is to eliminate the padded object
        const keys = Object.keys(value).sort().reverse().slice(1)
        // set the reference key
        const oldestKey = keys[keys.length - 1]

        // abort when all data has been loaded
        if (allItemsLoaded) return true

        // otherwise, set the data
        dispatch(productsAppendProducts(keys, value))
        dispatch(productsSetOldestKey(oldestKey))
        dispatch(productsIsFetched())
        // check if all data loaded
        if (keys.length < itemToLoad) dispatch(productsAllLoaded())
      }
    ).catch(
      /**
       * Use toast to notify about the error.
       * @param {string} code - Error code
       * @param {string} message - Error message
       */
      ({ code, message}) => {
        dispatch(productsIsFetched())
        dispatch(toastActions.toastErrorMessage(message, code))
      }
    )
  },

  /**
   * Toggle an item's like.
   * @param {string} key - Firebase ID
   */
  toggleLike: key => productsToggleItemLike(key),
}

export default types
export { actions, dispatchers }
