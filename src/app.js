import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import ProductsPage from './products'
import ProductPage from './product'
import Navbar from './navbar'
import { FOF_PATH } from './config'
import { NoRoute, NotFound, PropRoute } from './component'
import './app.css'

class App extends Component {
  /**
   * Render the app.
   */
  render() {
    return (
      <BrowserRouter>
        <div id="ssk-app" className="ssk app">
          <Switch>
            <PropRoute
              exact path={'/category/:category'}
              component={ProductsPage}
              isSubCategory={false}
            />

            <PropRoute
              exact path={'/category/:category/sub/:tag'}
              component={ProductsPage}
              isSubCategory={true}
            />

            <Route exact path={'/product/:productId'} component={ProductPage} />
            <Route exact path={FOF_PATH} component={NotFound} />

            <Route component={NoRoute} />
          </Switch>
          <Navbar />
        </div>
      </BrowserRouter>
    )
  }
}

export default App
